from typing import List, Optional

from fastapi.applications import FastAPI

from models import SubwayRoute, SubwayStop
from services import MbtaService

app = FastAPI()
# I should probably do some kind of static export from another file, but I'm at my time
service = MbtaService()


def format_route(route: dict) -> SubwayRoute:
    """
    Formats a route data dictionary into a python data class. This allows FastAPI to document our
    endpoint automatically in swagger

    Args:
        route (dict): A dictionary with the information for a MBTA Subway route

    Returns:
        SubwayRoute - A SubwayRoute data class
    """
    return SubwayRoute(id=route["id"], name=route["attributes"].get("long_name", ""))


def format_stop(stop: dict) -> SubwayStop:
    """
    Formats a stop data dictionary into a python data class. This allows FastAPI to document our
    endpoint automatically in swagger

    Args:
        stop (dict): A dictionary with the information for a MBTA Subway stop

    Returns:
        SubwayStop - A SubwayStop data class
    """
    return SubwayStop(id=stop["id"], name=stop["attributes"].get("name", ""))


@app.get("/routes", response_model=List[SubwayRoute])
async def routes_list(id: Optional[str] = None, page: int = 0, page_length: int = 25):
    routes = await service.get_routes(
        query_params={"id": id, "page": page, "page_length": page_length}
    )
    return [format_route(route) for route in routes]


@app.get("/stops", response_model=List[SubwayStop])
async def stops_list(route: Optional[str] = None, page: int = 0, page_length: int = 25):
    stops = await service.get_stops(
        query_params={"route": route, "page": page, "page_length": page_length}
    )
    return [format_stop(stop) for stop in stops]
