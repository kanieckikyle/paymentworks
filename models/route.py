from pydantic import BaseModel


class SubwayRoute(BaseModel):
    id: str
    name: str
