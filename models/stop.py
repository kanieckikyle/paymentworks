from pydantic import BaseModel


class SubwayStop(BaseModel):
    id: str
    name: str
