from typing import Dict, List, Optional, Union
from urllib.parse import urlencode

from httpx import AsyncClient


class MbtaService:
    """
    A facade service over the MBTA api to make it a little easier to work with. Currently,
    this service is read only.

    Attributes:
        base_url (str): The base url scheme to prepend to endpoints when sending requests.
            This should include the scheme (http/https)
    """

    base_url = "https://api-v3.mbta.com/"

    client = AsyncClient()

    async def get_routes(self, **kwargs):
        """
        Get a list of train routes in the MBTA train system

        Args:
            query_params (dict: optional): a dictionary of query parameters to filter results

        Returns:
            list - A list of MBTA routes
        """
        response = await self.client.get(self.get_url("routes", **kwargs))
        return self._format_response(response.json())

    async def get_stops(self, **kwargs):
        """
        Get a list of stops in the MBTA train system

        Args:
            query_params (dict: optional): a dictionary of query parameters to filter results

        Returns:
            list - A list of MBTA stops
        """
        response = await self.client.get(self.get_url("stops", **kwargs))
        return self._format_response(response.json())

    def get_url(self, endpoint: str, query_params: Optional[Dict] = None) -> str:
        """
        Builds and returns a MBTA api url that is ready to send in a request. This function will take care
        of formatting any query parameters into the appropriate format

        Args:
            endpoint (str): The endpoint that will be appended to the end of the base url
            query_params (dict: optional): a dictionary of query parameters to filter results

        Returns:
            str - A valid MBTA api url
        """
        # In the format, I use the 'or' syntax since a default value of an empty dictionary
        # is unsafe, but I still want to have the ability to give no query params
        return "{}{}{}".format(
            self.base_url, endpoint, self._format_query_params(query_params or {})
        )

    def _format_response(self, data: dict) -> Union[List, Dict]:
        return data.get("data")

    def _format_query_params(self, params: Dict) -> str:
        # First, get the page and the page length, using decent defaults
        # https://api-v3.mbta.com/docs/swagger/index.html#/Stop/ApiWeb_StopController_index
        page = params.pop("page", 0)
        page_length = params.pop("page_length", 25)
        parts = ["page[offset]={}".format(page), "page[limit]={}".format(page_length)]
        # If we have any other params left, let's add them to the end
        if params:
            parts.append(
                urlencode(
                    {
                        "filter[{}]".format(key): value
                        for key, value in params.items()
                        if value is not None
                    },
                    safe="[]",
                )
            )

        # Just in case our query params have only null values
        # then let's not have a trailing '&' at the end of our url
        return "?{}".format("&".join(filter(None, parts)))
