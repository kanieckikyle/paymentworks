import json
from typing import Callable, List, Optional
from unittest.mock import AsyncMock, patch

import pytest
from httpx import Response

from services import MbtaService


@pytest.fixture
def mbta_service():
    return MbtaService()


@pytest.fixture
def response_factory() -> Callable[[dict, Optional[int]], Response]:
    def get_response(data: List[dict], status_code=200) -> Response:
        return Response(status_code, json={"data": data})

    return get_response


@pytest.mark.asyncio
async def test_get_stops(mbta_service: MbtaService, response_factory):
    expected = {
        "attributes": {
            "address": None,
            "at_street": None,
            "description": "Prudential - Prudential Center",
            "latitude": 42.345632,
            "location_type": 2,
            "longitude": -71.081223,
            "municipality": "Boston",
            "name": "Prudential - Prudential Center",
            "on_street": None,
            "platform_code": None,
            "platform_name": None,
            "vehicle_type": None,
            "wheelchair_boarding": 1,
        },
        "id": "door-prmnl-prudential",
        "links": {"self": "/stops/door-prmnl-prudential"},
        "relationships": {
            "child_stops": {},
            "facilities": {
                "links": {"related": "/facilities/?filter[stop]=door-prmnl-prudential"}
            },
            "parent_station": {"data": {"id": "place-prmnl", "type": "stop"}},
            "recommended_transfers": {},
            "zone": {"data": None},
        },
        "type": "stop",
    }
    with patch(
        "httpx.AsyncClient.get",
        AsyncMock(return_value=response_factory([expected])),
    ) as http_get_mock:
        stops = await mbta_service.get_stops()
        assert json.dumps(stops[0]) == json.dumps(expected)
        http_get_mock.assert_awaited_once()


@pytest.mark.asyncio
async def test_get_routes(mbta_service: MbtaService, response_factory):
    expected = {
        "attributes": {
            "color": "DA291C",
            "description": "Rapid Transit",
            "direction_destinations": ["Mattapan", "Ashmont"],
            "direction_names": ["Outbound", "Inbound"],
            "fare_class": "Rapid Transit",
            "long_name": "Mattapan Trolley",
            "short_name": "",
            "sort_order": 10011,
            "text_color": "FFFFFF",
            "type": 0,
        },
        "id": "Mattapan",
        "links": {"self": "/routes/Mattapan"},
        "relationships": {
            "line": {"data": {"id": "line-Mattapan", "type": "line"}},
            "route_patterns": {},
        },
        "type": "route",
    }
    with patch(
        "httpx.AsyncClient.get",
        AsyncMock(return_value=response_factory([expected])),
    ) as http_get_mock:
        routes = await mbta_service.get_routes()
        assert json.dumps(routes[0]) == json.dumps(expected)
        http_get_mock.assert_awaited_once()


def test_get_url_starts_with_base_url(mbta_service: MbtaService):
    url = mbta_service.get_url("test")
    # Use startswith to ignore query params, since we just want to make sure we are getting the right
    # endpoint
    assert url.startswith(mbta_service.base_url)


def test_get_url_endpoint_appended(mbta_service: MbtaService):
    url = mbta_service.get_url("routes")
    # Use startswith to ignore query params, since we just want to make sure we are getting the right
    # endpoint
    assert url.startswith("{}routes".format(mbta_service.base_url))


def test_get_url_query_params(mbta_service: MbtaService):
    url = mbta_service.get_url(
        "routes", query_params={"route": "red", "page": 23, "page_length": 50}
    )
    # Make sure the query parameters are formatted according to
    assert "filter[route]=red" in url
    assert "page[offset]=23" in url
    assert "page[limit]=50" in url
